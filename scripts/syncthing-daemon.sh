#!/bin/bash -xe

gui_addr_file="$SNAP_COMMON/gui-address"

get_gui_address() {
  xmlstarlet sel -t -v "/configuration/gui/address" "$XDG_CONFIG_HOME/syncthing/config.xml"
}

trap "rm -f $gui_addr_file" EXIT

export HOME="$SNAP_COMMON"
export XDG_CONFIG_HOME="$SNAP_COMMON"
export STNOUPGRADE="1"

if [ -f "$SNAP_USER_COMMON/syncthing/config.xml" ]; then
  # This is the old daemon config location (SNAP_USER_COMMON).
  # If we upgrade from an old installation, 
  # do not use the new default location (SNAP_COMMON).
  export HOME="$SNAP_USER_COMMON"
  export XDG_CONFIG_HOME="$SNAP_USER_COMMON"
fi

nice -n 9 $SNAP/bin/syncthing -no-browser -no-restart &

echo "$(get_gui_address)" > "$gui_addr_file"
chmod a+r "$gui_addr_file"

wait
