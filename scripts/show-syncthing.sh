#!/bin/bash -xe

first_run_marker="$SNAP_USER_COMMON/is-not-first-run"

is_syncthing_running_as() {
  pgrep -x syncthing -u $1 > /dev/null
}

is_first_run() {
  test ! -f "$first_run_marker"
}

is_daemon_running() {
  test -f "$SNAP_COMMON/gui-address"
}

user_wants_autostart() {
  $SNAP/bin/http-prompt $SNAP/prompt-for-autostart.html
}

setup_autostart() {
  touch "$first_run_marker"
  if [ "$(user_wants_autostart)" = "yes" ]; then
    $SNAP/bin/enable-autostart.sh
  fi
}

get_root_gui_url() {
  echo "http://$(cat $SNAP_COMMON/gui-address)"
}

launch_syncthing() {
  if is_syncthing_running_as $UID; then 
    $SNAP/bin/launch-syncthing.sh -browser-only
  else
    $SNAP/bin/launch-syncthing.sh \
        -logfile="$SNAP_USER_COMMON/log.txt" \
        "$@" \
        &> /dev/null &
  fi
}

show_user_ui() {
  if is_first_run; then
    # the autostart prompt will forward the user to syncthing
    launch_syncthing -no-browser
    setup_autostart
  else
    launch_syncthing
  fi
}

show_daemon_ui() {
  xdg-open "$(get_root_gui_url)"
}

if is_syncthing_running_as $UID; then
  show_user_ui
elif is_daemon_running; then
  show_daemon_ui
else
  show_user_ui
fi
